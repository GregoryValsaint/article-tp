<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', null, [
                'required' => true,
            ])
            ->add('date_publication', null,[
                'label' => 'date de creation :',
                'required'   => false,
                'empty_data' => '20-06-2020 00:00:00',
            ])
            ->add('contenu', null,[
                'required'   => true,
                'help' => 'Explain your mind on the marvelous year of all the time: 2020',
            ])
            ->add('submit', SubmitType::class);
    }
}
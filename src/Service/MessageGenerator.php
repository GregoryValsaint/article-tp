<?php


namespace App\Service;


use App\Repository\ArticleRepository;
use Psr\Log\LoggerInterface;

class MessageGenerator
{
    private $aRepository;
    private $logger;
    private $logEnabled;
    /**
     * MessageGenerator constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository, LoggerInterface $logger, $logEnabled){
        $this->logEnabled = $logEnabled;
        $this->logger = $logger;
        $this->aRepository = $articleRepository;
    }
    public function getMessage(){
        $messages = [
            "Salut à tous",
            "Comment ca va ?",
            "Hello toi"
        ];
        $message = $messages[rand(0,2)];
        if ($this->logEnabled){
            $this->logger->notice("Prout Prout: '$message'");
        }
        return $message;
    }
    public function getPublishedArticlesTitle(){
        $titles = [];
        $articles = $this->aRepository->getPublishedArticles();
        foreach ($articles as $article) {
            $titles[]=$article->getTitre();
        }
        return $titles;
    }

}
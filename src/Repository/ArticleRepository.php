<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    /**
     * @return Article[]
     */
    public function getThisYearArticles(){
        return $this->createQueryBuilder("a")
            ->where("a.date_publication > '2020-01-01'")
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Article[]
     */
    public function getPublishedArticles(){
        return $this->createQueryBuilder("art")
            ->where("art.date_publication <= :current_date")
            ->setParameter("current_date", new \DateTime())
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * search an article by title or content
     * @param string $userSearch string to search
     * @param string $order posts order
     * @return Article[] search results
     */
    public function searchArticles($search, $order){
        return $this->createQueryBuilder("result")
            ->where("result.titre like :search")
            ->orWhere("result.contenu like :search")
            ->orderBy("result.date_publication", $order)
            ->setParameter("search", "%".$search."%")
            ->getQuery()  
            ->getResult()
            ;
    }
}

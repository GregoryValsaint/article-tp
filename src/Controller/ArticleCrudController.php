<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticleCrudController extends AbstractController
{
    /**
     * @Route("/articles", name="articles_index")
     */
    public function index(ArticleRepository $repository)
    {
        $articles = $repository->findAll();
        return $this->render('article_crud/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/articles/create", name="articles_create")
     */
    public function create(Request $request){
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        dump($article);
        if($form->isSubmitted() && $form->isValid()){
            // Vérifie que le form a bien été envoyé et valide
            // je peux sauvegarder mon utilisateur
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute("articles_read",["article"=>$article->getId()]);
        }
        return $this->render("article_crud/create.html.twig", [
            'myForm'=>$form->createView()
        ]);

    }

    /**
     * @Route("/articles/{article}", name="articles_read")
     */

    public function read(Article $article){

        return $this->render('article_crud/read.html.twig', [
            "article" => $article
        ]);
    }

    /**
     * @Route("/articles/{article}/update", name="articles_update")
     */

    public function update(Article $article, Request $request){
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        dump($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute("articles_read", ["article"=>$article->getId()]);
        }
        return $this->render('article_crud/update.html.twig', [
            "articleForm" => $form->createView()
        ]);
    }



    /**
     * @Route("/articles/{article}/delete", name="articles_delete")
     */

    public function delete(Article $article){

        $em = $this->getDoctrine()->getManager();
        $em ->remove($article);
        $em->flush();
        return $this->redirectToRoute("articles_index");
    }
}

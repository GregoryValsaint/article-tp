<?php

namespace App\Controller;

use App\Form\ResearchType;
use App\Repository\ArticleRepository;
use App\Service\MessageGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/blog")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("/", name="blog_index", methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository, MessageGenerator $messageGenerator)
    {
        $titles = $messageGenerator->getPublishedArticlesTitle();
        $message = $messageGenerator->getMessage();
        $articles = $articleRepository->getThisYearArticles();
        return $this->render('blog/index.html.twig', [
            'articles' => $articles,
            'message' => $message,
            'titles' => $titles
        ]);
    }

    /**
     * @Route("/search", name="blog_search")
     */
    public function search(Request $request, ArticleRepository $articleRepository)
    {
        $form = $this->createForm(ResearchType::class, null, ["method" => "GET"]);
        $form->handleRequest($request);
        $userSearch = $form->get("search")->getData();
        $order = $form->get("order")->getData();


        $articles = $articleRepository->searchArticles($userSearch, $order);
        dump($form->get("search")->getData());
        return $this->render('blog/search.html.twig', [
            "searchForm" => $form->createView(),
            "articles" => $articles
        ]);
    }
}
